import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { authGuard } from './auth.guard';
import { AboutUsComponent } from './about-us/about-us.component';
import { HomepageComponent } from './homepage/homepage.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { RefundsComponent } from './refunds/refunds.component';
import { TermsComponent } from './terms/terms.component';
import { ProductsComponent } from './products/products.component';
import { ContactusComponent } from './contactus/contactus.component';
import { LogoutComponent } from './logout/logout.component';


const routes: Routes = [
  {path: '', component: HomepageComponent },
  {path:'login',       component:LoginComponent},
  {path:'registration', component:RegistrationComponent},
  {path:'about-us', component:AboutUsComponent},
  {path:'products', component:ProductsComponent},
  {path:'privacy', component:PrivacyComponent},
  {path:'refunds', component:RefundsComponent},
  {path:'terms', component:TermsComponent},
  {path:'homepage', component:HomepageComponent},
  {path:'contactus', component:ContactusComponent},
  {path:'logout', component:LogoutComponent}


];

@NgModule({
  
 
    imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled', anchorScrolling: 'enabled' })],
    exports: [RouterModule]
 
 
  

})
export class AppRoutingModule { }