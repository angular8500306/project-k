import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  loginStatus: any;
  isUserLogged: any;
  
  constructor(private http: HttpClient) {
    this.loginStatus = false;
    this.isUserLogged = new Subject();
  }
  getLoginStatus(): boolean {
    return this.loginStatus;
  }
  setLoginStatus() {
    this.loginStatus = true;
    this.isUserLogged.next(true);
  }
  getIsUserLogged(): any {
    return this.isUserLogged.asObservable();
  }
  setLogoutStatus() {
    this.loginStatus = true;
    this.isUserLogged.new(true);
  }
  getLogoutStatus(): boolean {
    return this.loginStatus;
  }
  getIsUserLogout(): any {
    return this.isUserLogged.asObservable();
  }


  registerUser(User: any): any {
    return this.http.post('http://localhost:8085/addSignup', User);
  }
  userLogin(EmailId: any, Password: any): any {
    return this.http.get('http://localhost:8085/getLogin/' + EmailId + '/' + Password).toPromise();
  }


}
