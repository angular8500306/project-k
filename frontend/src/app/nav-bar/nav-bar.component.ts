import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent  {
  email:  string = '';

  constructor( private router: Router) {}

  loggedIn(){
  }

  signout(){
    this.router.navigate(['/login']);
    
  }
  admin(){
    if(this.email=='admin'){
      return true;
    }
    else{
      return false;
    }
  }
  
 
}
