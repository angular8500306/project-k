package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class User {
	@Id@GeneratedValue
	private int sno;
	private String FirstName;
	private String LastName;
	private long PhoneNumber;
	private String EmailId;
	private String Password;
//	private String ConformPassword;
	
	

	public User(String FirstName, String LastName,long PhoneNumber
			, String EmailId,String Password) {
		super();
		this.FirstName = FirstName;
		this.LastName = LastName;
		this.PhoneNumber = PhoneNumber;
		this.EmailId = EmailId;
		this.Password = Password;
//		this.ConformPassword = ConformPassword;
	}
	public User() {
		super();
	}

	public int getSno() {
		return sno;
	}
	public void setSno(int sno) {
		this.sno = sno;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public long getPhoneNumber() {
		return PhoneNumber;
	}
	public void setPhoneNumber(long phoneNumber) {
		PhoneNumber = phoneNumber;
	}
	public String getEmailId() {
		return EmailId;
	}
	public void setEmailId(String emailId) {
		EmailId = emailId;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
//	public String getConformPassword() {
//		return ConformPassword;
//	}
//	public void setConformPassword(String conformPassword) {
//		ConformPassword = conformPassword;
//	}


	

	@Override
	public String toString() {
		return "User [FirstName=" + FirstName + ", LastName=" + LastName + ", phoneNumber=" + PhoneNumber
				+ ", emailId=" + EmailId + ", password=" + Password +"]";
	}
}



