package com.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.model.SecurityConfig;

import java.util.List;

import org.mindrot.jbcrypt.BCrypt;
import com.model.User;

@Service
public class UserDao {

	@Autowired
    UserRepository userRepo;
	@Autowired
	private JavaMailSender javaMailSender;
	
	public User addSignup(User user) {
		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
		String encryptedPassword = bcrypt.encode(user.getPassword());
		user.setPassword(encryptedPassword);
		User savedUser = userRepo.save(user);
		sendWelcomeEmail(savedUser.getEmailId());
		return savedUser;

	}
	private void sendWelcomeEmail(String email) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(email);
		message.setSubject("Welcome to RostedRoyal");
		message.setText("Thank you for registering with RostedRoyal. We hope you enjoy our Popular Coffee Beans!");
		javaMailSender.send(message);
	}

	public User getLogin(String EmailId, String Password) {
		User user=userRepo.findByName(EmailId);
		if(EmailId!=null&& BCrypt.checkpw(Password, user.getPassword())){
			return user;
		}
		
		else{
			return null;
		}
	}
	

}


