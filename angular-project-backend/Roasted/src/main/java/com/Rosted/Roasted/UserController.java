package com.Rosted.Roasted;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.model.SecurityConfig;
import org.mindrot.jbcrypt.BCrypt;
import com.dao.UserDao;
import com.model.User;


@CrossOrigin(origins="http://localhost:4200")
@RestController
@Service
public class UserController {
	
	@Autowired
	UserDao userDao;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	
	@GetMapping("getLogin/{EmailId}/{Password}")
	public User getLogin(@PathVariable("EmailId") String EmailId,
			@PathVariable("Password") String Password) {
		return userDao.getLogin(EmailId, Password);
	}

	@PostMapping("addSignup")
	public User addSignup(@RequestBody User user) {
		return userDao.addSignup(user);
	}
	

}

